import Vue from 'vue'
import App from './app'
import CodeBlock from './demo-components/code-block'
import {VcParallax, VcParallaxSection} from './components/vc-parallax'

Vue.config.productionTip = false

Vue.use(VcParallax)
Vue.use(VcParallaxSection)
Vue.component('code-block', CodeBlock)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App)
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App)
})
