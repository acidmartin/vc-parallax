/**
 * Export VcParallax, VcParallaxSection and add to Vue
 * @module
 */

import VcParallax from './parallax'
import VcParallaxSection from './section'

VcParallax.install = function install (Vue) {
  Vue.component(VcParallax.name, VcParallax)
}

VcParallaxSection.install = function install (Vue) {
  Vue.component(VcParallaxSection.name, VcParallaxSection)
}

export {VcParallax, VcParallaxSection}
