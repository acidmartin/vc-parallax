/**
 * Demo navigation types of the component
 * @module navTypes
 */

const navTypes = [
  'buttons',
  'scroll'
]

export default navTypes
