/**
 * Demo navigation positions of the component
 * @module configurator
 */

const navPositions = [
  'top',
  'right',
  'bottom',
  'left'
]

export default navPositions
