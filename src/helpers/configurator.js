/**
 * Demo form fields
 * @module configurator
 */

import colors from './colors'
import navTypes from './nav-types'
import navPositions from './nav-positions'

const configurator = {
  watch: {
    'configurator.navType.model' () {
      this.configurator.navAccentColor.disabled = this.configurator.navType.model === 'buttons'
    }
  },
  data () {
    return {
      configurator: {
        navType: {
          label: 'nav-type',
          model: navTypes[0],
          type: 'select',
          colorPicker: false,
          items: navTypes,
          disabled: false
        },
        navPosition: {
          label: 'nav-position',
          model: navPositions[1],
          type: 'select',
          colorPicker: false,
          items: navPositions,
          disabled: false
        },
        navColor: {
          label: 'nav-color',
          model: colors[0],
          type: 'select',
          colorPicker: true,
          items: colors,
          disabled: false
        },
        navAccentColor: {
          label: 'nav-accent-color *',
          model: colors[1],
          type: 'select',
          colorPicker: true,
          items: colors,
          disabled: true
        },
        sectionColor: {
          label: 'section-color',
          model: colors[colors.length - 1],
          type: 'select',
          colorPicker: true,
          items: colors,
          disabled: false
        },
        sectionBgColor: {
          label: 'section-bg-color',
          model: colors[1],
          type: 'select',
          colorPicker: true,
          items: colors,
          disabled: false
        }
      }
    }
  }
}

export default configurator
