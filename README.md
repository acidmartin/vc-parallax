# vc-parallax

VcParallax is a lightweight component, based on VueJs for adding awesome parallax effects to web pages.

Website and demo: [https://parallax.wemakesites.net](https://parallax.wemakesites.net)

Repo: [https://bitbucket.org/acidmartin/vc-parallax](https://bitbucket.org/acidmartin/vc-parallax)

Author: [Martin Ivanov](https://wemakesites.net)

## Installation

```bash
// npm
$ npm install vc-parallax --save
// yarn
$ yarn add vc-parallax
```

## Adding to a VueJs Project
``` javascript
import {VcParallax, VcParallaxSection} from 'vc-parallax'
// import the stylesheet
import '../node_modules/vc-parallax/dist/lib/vc-parallax.min.css'
// add to Vue
Vue.use(VcParallax)
Vue.use(VcParallaxSection)
```

## Demo, configuration, events and props

[https://parallax.wemakesites.net](https://parallax.wemakesites.net)


## Development and Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
